require 'rails_helper'

RSpec.describe Reading, type: :model do
  describe 'kwh' do
    fixtures :readings, :registers
    let(:reading) { readings(:reading_01) }
    let(:reading_dec) { readings(:reading_03) }

    it 'is valid if it is equals to last reading' do
      reading.kwh = Reading.order('kwh').last.kwh
      expect(reading).to be_valid
    end

    it 'is valid if it is incremental' do
      reading.kwh = Reading.order('kwh').last.kwh + 1
      expect(reading).to be_valid
    end

    it 'is invalid if it is not incremental' do
      reading.kwh = Reading.order('kwh').last.kwh - 1
      expect(reading).to_not be_valid
    end

    it 'is invalid with decimals if no decimals expected' do
      reading.kwh = 2.01

      expect(reading).to_not be_valid
    end

    it 'is invalid without kwh' do
      reading.kwh = nil

      expect(reading).to_not be_valid
    end

    it 'is invalid with more significant decimals than expected' do
      reading_dec.kwh = 1.0011

      expect(reading_dec).to_not be_valid
    end

    it 'is valid with less significant decimals than expected' do
      reading_dec.kwh = 1.001

      expect(reading_dec).to be_valid
    end
  end
end
