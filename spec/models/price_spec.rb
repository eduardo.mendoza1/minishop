require 'rails_helper'

RSpec.describe Price, type: :model do
  describe 'Price' do
    fixtures :prices
    let(:price) { prices(:price_02) }

    it 'is valid' do
      expect(price).to be_valid
    end

    it 'is invalid without price_value' do
      price.price_value = nil

      expect(price).to_not be_valid
    end
  end
end
