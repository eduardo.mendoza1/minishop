require 'rails_helper'

RSpec.describe User do
  let(:user) { User.new }

  describe 'User' do
    fixtures :addresses
    it 'is invalid without #username' do
      expect(user).to_not be_valid
    end

    it 'is invalid without #address' do
      user.username = 'I am a name'

      expect(user).to_not be_valid
    end

    it 'is valid' do
      user.username = 'I am a name'
      user.address = addresses(:address_02)

      expect(user).to be_valid
    end
  end

  describe '#kwh_billed_till is consistent with active Bills' do
    fixtures :users, :bills
    let(:user) { users(:user_02) }
    it 'is equals to the sum of all active bills till date' do
      expect(user.kwh_billed_till(Time.current)).to eq(bills(:bill_01).kwh_billed + bills(:bill_02).kwh_billed)
    end
  end

  describe '#kwh_to_be_billed_till is consistent with Readings' do
    fixtures :readings, :users
    let(:user) { users(:user_01) }
    it 'is equals to last Reading till date, minus Bills till date' do
      expect(user.kwh_to_be_billed_till(Time.current)).to eq(readings(:reading_03).kwh)
    end
  end

  describe '#bill_amount_by_date is consistent with Readings and Price values' do
    fixtures :readings, :users, :prices
    let(:user) { users(:user_01) }
    it 'is equals to pending Bills till date multiplied by Price valid till date' do
      expect(user.bill_amount_by_date(Time.current)).to eq(readings(:reading_03).kwh * prices(:price_02).price_value)
    end
  end
end
