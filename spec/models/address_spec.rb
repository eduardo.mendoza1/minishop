require 'rails_helper'

RSpec.describe Address do
  let(:address) { Address.new }

  describe 'Address' do
    fixtures :addresses
    let(:one) { addresses(:address_01) }

    it 'is invalid without #number' do
      address.street = one.street
      address.postcode = one.postcode
      address.icp = one.icp + '0'

      expect(address).to_not be_valid
    end

    it 'is invalid without #street' do
      address.number = one.number
      address.postcode = one.postcode
      address.icp = one.icp + '0'

      expect(address).to_not be_valid
    end

    it 'is invalid without #postcode' do
      address.number = one.number
      address.street = one.street
      address.icp = one.icp + '0'

      expect(address).to_not be_valid
    end

    it 'is invalid without #icp' do
      address.number = one.number
      address.street = one.street
      address.postcode = one.postcode

      expect(address).to_not be_valid
    end

    it 'is invalid if #icp already exists' do
      address.number = one.number
      address.street = one.street
      address.postcode = one.postcode
      address.icp = one.icp
      address.valid?

      expect(address.errors[:icp]).to include('has already been taken')
    end
  end

  describe '#kwh_used_till is consistent with Readings' do
    fixtures :addresses, :readings
    it 'is equals to last Reading till date' do
      expect(addresses(:address_02).kwh_used_till(Time.current)).to eq(readings(:reading_02).kwh)
    end
  end
end
