require 'rails_helper'

RSpec.describe Register, type: :model do
  describe 'Register' do
    fixtures :registers, :addresses
    let(:register) { registers(:register_02) }

    it 'is valid' do
      expect(register).to be_valid
    end

    it 'is valid without register_decimals' do
      register.register_decimals = 0

      expect(register).to be_valid
    end

    it 'is invalid without address' do
      register.address = nil

      expect(register).to_not be_valid
    end

    it 'is invalid without meter_number' do
      register.meter_number = ''

      expect(register).to_not be_valid
    end

    it 'is invalid with meter_number too long' do
      register.meter_number = 'ABCDEF1234567890'

      expect(register).to_not be_valid
    end

    it 'is invalid with register_number too long' do
      register.register_number = 'ABC'

      expect(register).to_not be_valid
    end

    it 'is invalid without register_number' do
      register.register_number = nil

      expect(register).to_not be_valid
    end
  end
end
