require 'rails_helper'

RSpec.describe Bill, type: :model do
  describe 'Bill' do
    fixtures :bills, :users
    let(:bill) { bills(:bill_02) }
    let(:user) { users(:user_01) }

    it 'is valid' do
      expect(bill).to be_valid
    end

    it 'is invalid without user' do
      bill.user = nil

      expect(bill).to_not be_valid
    end

    it 'is invalid without #kwh_billed' do
      bill.kwh_billed = nil

      expect(bill).to_not be_valid
    end
  end

  describe '#make_me_the_last works consistently with all bills' do
    fixtures :bills, :users
    let(:bill) { bills(:bill_01) }
    let(:user) { users(:user_02) }
    it 'has more than one bill before making it the last' do
      expect(user.kwh_billed_till(Time.current)).to_not eq(bill.kwh_billed)
    end
    it 'chages status to <false> to subsequent bills' do
      bill.make_me_the_last
      expect(user.kwh_billed_till(Time.current)).to eq(bill.kwh_billed)
    end
  end
end
