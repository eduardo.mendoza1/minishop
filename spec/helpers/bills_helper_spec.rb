require 'rails_helper'

RSpec.describe BillsHelper do
  describe '#date_last_bill_by_user' do
    fixtures :users, :bills
    it 'is equals to last active bill for that user' do
      expect(helper.date_last_bill_by_user(users(:user_02))).to eq(bills(:bill_02).billed_at.to_formatted_s(:long_ordinal))
    end

    it 'is equals to message if there is no active bill' do
      Bill.delete_all
      expect(helper.date_last_bill_by_user(users(:user_02))).to eq('No date available')
    end
  end
end
