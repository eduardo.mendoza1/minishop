require 'rails_helper'

RSpec.describe PricesHelper do
  describe '#price_link_string' do
    fixtures :prices
    it 'is equals to last valid price' do
      expect(helper.price_link_string).to eq(prices('price_02').price_value.to_s)
    end

    it 'is equals to message if there is no valid price' do
      Price.delete_all
      expect(helper.price_link_string).to eq('+ Add Price')
    end
  end
end
