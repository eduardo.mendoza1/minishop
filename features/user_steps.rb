When(/^I go to the Log up page$/) do
  visit new_user_path
end

Then(/^I should see a form to insert my data$/) do
  expect(page).to have_content('Username')
  expect(page).to have_content('Number')
  expect(page).to have_content('Street')
  expect(page).to have_content('Town')
  expect(page).to have_content('City')
  expect(page).to have_content('Postcode')
  expect(page).to have_content('ICP')
  expect(page).to have_content('Create User')
end

When(/^I press "Log up" link$/) do
  click_link 'Log up'
end

When(/^I can fill up the data$/) do
  fill_in 'Username', with: 'My User Name'
  fill_in 'Number', with: 'My House Number'
  fill_in 'Street', with: 'My Street Name'
  fill_in 'Postcode', with: '12345'
  fill_in 'ICP', with: 'My ICP Number'
end

When(/^I cancel the process$/) do
  click_link 'Cancel'
end

Then(/^I can create a user$/) do
  click_button 'Create User'
  expect(page).to have_content('Sorry, the information is being verified')
end
