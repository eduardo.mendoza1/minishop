Then(/^I want to have the option to create a user$/) do
  expect(page).to have_content('Log up')
end

When(/^I press Log up link$/) do
  visit root_path
  click_on('Log up')
end

Then(/^It should send me to New User page$/) do
  expect(page).to have_current_path(new_user_path)
end

When('I introduce {string} as username') do |username|
  fill_in 'Username', with: username.to_s
end

When('I start session as {string}') do |username|
  steps %(
          Given I go to the homepage
          And I introduce "#{username}" as username
          Then I click button "Log In"
         )
end
