When(/^I go to the homepage$/) do
  visit root_path
end

Then(/^I should see the login page$/) do
  expect(page).to have_content('Login')
end

When('I fill {string} with {string} value') do |field, value|
  fill_in field.to_s, with: value.to_s
end

When('I click button {string}') do |button_name|
  click_button button_name.to_s
end

When('I click link with href {string}') do |link_name|
  find(:xpath, "//a[@href='#{link_name}']").click
end

When('I click first link {string}') do |name|
  first(:link, name.to_s).click
end

When('I see the number {int}') do |number|
  expect(page).to have_content(number.to_s)
end

When('I see the number {float}') do |number|
  expect(page).to have_content(number.to_s)
end

When('I see the text {string}') do |text|
  expect(page).to have_content(text)
end

When('I cannot see the text {string}') do |text|
  expect(page).to_not have_content(text)
end
