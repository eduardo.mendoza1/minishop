# feature/user_cucumber.feature
Feature: User
  As a prospective customer
  I want to be able to introduce my address to ask for electricity supply
  So that my information can be assessed

  Scenario: User is visiting Log up page
    When I go to the Log up page
    Then I should see a form to insert my data

  Scenario: User can cancel Log up process
    Given I go to the homepage
    When I press "Log up" link
    And I cancel the process
    Then I should see the login page

  Scenario: Prospective customer creates new user
    Given I go to the homepage
    When I press "Log up" link
    And I can fill up the data
    Then I can create a user
