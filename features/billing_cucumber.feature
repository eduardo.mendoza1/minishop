# feature/billing_cucumber.feature
Feature: Creating invoice
  As an Admin
  I want to be able to set a price
  So that I can invoice a customer

  Scenario: Admin End to End
    Given I start session as "admin"
    When I see the text "Clients"
    And I set a new price with the value 1.0
    And I create a reading with a value of 1000.0 kwh
    And I cannot see the text "Invoice $0.0"
    And I see the text "Invoice $1000.0"
    And I create an invoice for $1000.0
    Then I see the text "Invoice $0.0"
