# feature/login_cucumber.feature
Feature: Login
  As a user
  I want to start in a login page
  So that I can start a session or create a user
  
  Scenario: User is visiting homepage
    When I go to the homepage
    Then I should see the login page

  Scenario: User wants to have the option to create a new user
    When I go to the homepage
    Then I want to have the option to create a user

  Scenario: Log up should go to new user page
    When I press Log up link
    Then It should send me to New User page
