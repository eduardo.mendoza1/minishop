When('I see Invoice value equals to {float}') do |float|
  expect(page).to have_content("Invoice $#{float}")
end

When('I create an invoice for ${float}') do |amount|
  steps %(
          When I click first link "Invoice $#{amount}"
          Then I click button "Create Bill"
         )
end
