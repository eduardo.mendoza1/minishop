# feature/adding_reading_cucumber.feature
Feature: Inserting Reading 
  As a User
  I want to be able to enter kwh readings
  So that I can update the readings

  Scenario: Admin End to End 
    Given I start session as "admin"
    When I see the text "Clients"
    And I set a new price with the value 3.99
    And I create a reading with a value of 2.001 kwh
    Then I see Invoice value equals to 7.98399

  Scenario: User End to End
    Given I start session as "user02"
    And I see the text "User user02"
    When I create a reading with a value of 5.0 kwh
    Then I see the number 5
