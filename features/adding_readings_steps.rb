When('I set a new price with the value {float}') do |price_value|
  steps %(
          When I click link with href "/prices/new"
          And I fill "Price value" with "#{price_value}" value
          And I click button "Create Price"
         )
end

When('I create a reading with a value of {float} kwh') do |kwh|
  steps %(
          When I click first link "+ Add Reading"
          And I fill "Kwh" with "#{kwh}" value
          Then I click button "Create Reading"
         )
end
