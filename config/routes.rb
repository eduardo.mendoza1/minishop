Rails.application.routes.draw do
  root 'login#index'

  resources :login, only: [:index, :new]
  resources :users, only: [:new, :create]
  scope module: :admin do
    resources :addresses, only: [:index]
    resources :users, only: [:update]
    resources :registers, only: [:new, :create]
    resources :bills, only: [:new, :create]
    resources :prices, only: [:new, :create]
  end
  scope module: :logged do
    resources :readings, only: [:new, :create]
    resources :users, only: [:show] do
      get 'search', on: :collection
    end
  end
end
