class Address < ApplicationRecord
  has_one :user
  has_many :registers
  validates :number, presence: true
  validates :street, length: { minimum: 2, maximum: 200 }
  validates :postcode, numericality: true
  validates :icp, length: { minimum: 9, maximum: 15 }, uniqueness: true

  def kwh_used_till(date)
    registers.joins(:readings).where('read_at <= ?', date).group(:register_id).maximum(:kwh).values.sum
  end
end
