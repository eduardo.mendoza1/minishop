class Price < ApplicationRecord
  validates :price_value, presence: true, numericality: true

  scope :valid_till, ->(date) { where('valid_since <= ?', date).order('valid_since DESC') }

  def self.valid_on(date)
    valid_till(date)&.first
  end

  def self.price_by_date(date)
    price = valid_on(date)
    price.nil? ? 0 : price.price_value
  end

  def self.new_price_with_current_price
    current_price = valid_on(Time.current)
    current_price ? new(price_value: current_price.price_value) : new
  end
end
