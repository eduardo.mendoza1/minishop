class Register < ApplicationRecord
  has_many :readings
  belongs_to :address
  validates :meter_number, presence: true, length: { minimum: 1, maximum: 15 }
  validates :register_number, presence: true, length: { minimum: 1, maximum: 2 }, uniqueness: { scope: [:meter_number, :address] }
  validates :register_decimals, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 4 }
end
