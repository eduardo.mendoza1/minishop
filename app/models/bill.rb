class Bill < ApplicationRecord
  belongs_to :user
  belongs_to :price
  validates :kwh_billed, presence: true, numericality: true
  validates :billed_at, presence: true

  scope :active, -> { where(status: true) }
  scope :billed_till, ->(date) { where('billed_at <= ?', date) }
  scope :billed_after, ->(date) { where('billed_at > ?', date) }

  def make_me_the_last
    Bill.billed_after(billed_at).update_all(status: false)
  end
end
