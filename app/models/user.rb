class User < ApplicationRecord
  has_many :bills
  belongs_to :address
  accepts_nested_attributes_for :address
  validates :username, presence: true, uniqueness: true

  def kwh_billed_till(date)
    bills.active.billed_till(date).sum(&:kwh_billed)
  end

  def kwh_to_be_billed_till(date)
    address.kwh_used_till(date) - kwh_billed_till(date)
  end

  def bill_amount_by_date(date)
    BigDecimal(Price.price_by_date(date) * kwh_to_be_billed_till(date), 2)
  end
end
