class Reading < ApplicationRecord
  belongs_to :register
  validates :kwh, presence: true, numericality: true
  validate :validate_kwh_is_greater_or_eq
  validate :validate_kwh_format

  scope :read_till, ->(date) { where('read_at <= ?', date) }

  private

  def validate_kwh_format
    decimals = register.register_decimals
    str_without_dec = '\\A\\d+\\.0\\z'
    str_with_dec = '\\A\\d+(?:\\.\\d{0,' + decimals.to_s + '})?\\z'
    regexp = decimals.positive? ? str_with_dec : str_without_dec

    (errors.add :kwh, (decimals.to_s + ' decimals expected, but got: ' + kwh.to_s)) if kwh.to_s !~ Regexp.new(regexp)
  end

  def validate_kwh_is_greater_or_eq
    gt_reading_register
    # stop validation if kwh is NULL or no record exists with greater value
    return if kwh.nil? || !self.class.where(@reading_kwh_gt).where(@register_by_id).exists?

    errors.add :kwh, ' must be greater or equals than the others stored in existing records'
  end

  def gt_reading_register
    @reading_kwh_gt = self.class.arel_table[:kwh].gt(kwh)
    @register_by_id = self.class.arel_table[:register_id].eq(register_id)
  end
end
