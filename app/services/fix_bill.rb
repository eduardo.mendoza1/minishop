# app/services/fix_bill.rb
class FixBill
  def initialize(bill)
    @bill = bill
  end

  def call
    groom_bill_values
  end

  private

  def groom_bill_values
    @bill.billed_at = Time.current if @bill.billed_at.nil?
    @bill.price = Price.valid_on(@bill.billed_at)
  end
end
