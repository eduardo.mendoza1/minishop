# app/helpers/bills_helper.rb

module BillsHelper
  def date_last_bill_by_user(user)
    user.bills.active.last&.billed_at&.to_formatted_s(:long_ordinal) || 'No date available'
  end
end
