# app/helpers/prices_helper.rb

module PricesHelper
  def price_link_string
    price = Price.valid_on(Time.current)
    price ? price.price_value.to_s : '+ Add Price'
  end
end
