module Logged
  class ReadingsController < SessionBaseController
    def new
      @reading = Reading.new(register: Register.find(params[:register_id]))
    rescue ActiveRecord::RecordNotFound
      redirect_to '/404'
    end

    def create
      @reading = Reading.new(reading_params)

      if @reading.save
        redirect_to back_from_level_two
      else
        render 'new'
      end
    end

    private

    def reading_params
      params.require(:reading).permit(:kwh, :read_at, :register_id)
    end
  end
end
