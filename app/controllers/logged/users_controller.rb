module Logged
  class UsersController < SessionBaseController
    def show
      @user = User.find(params[:id])
    end

    def search
      @user = User.find_by(username: params[:username])

      if @user.nil?
        flash[:notice] = "User #{params[:username]} not found"
        redirect_to root_url
      else
        redirect_to @user
      end
    end
  end
end
