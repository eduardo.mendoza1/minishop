class SessionBaseController < UsersBaseController
  before_action :check_session_user

  def check_session_user
    redirect_to '/422' unless session?
  end
end
