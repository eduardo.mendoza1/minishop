class UsersBaseController < ApplicationController
  def admin?
    session[:username] == 'admin'
  end

  def session?
    session[:username].present?
  end

  def start_session
    session[:username] = params[:username] || params[:user][:username]
  end

  def destroy_session
    session.delete(:username)
  end

  def back_from_level_two
    admin? ? addresses_url : search_users_path(username: session[:username])
  end
end
