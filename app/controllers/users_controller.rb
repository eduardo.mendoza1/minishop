class UsersController < UsersBaseController
  def new
    @user = User.new

    @user.build_address
  end

  def create
    @user = User.new(user_params)

    if @user.save
      start_session
      redirect_to @user
    else
      render 'new'
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :status, address_attributes: [:number, :street, :town_suburb, :region_city, :postcode, :icp])
  end
end
