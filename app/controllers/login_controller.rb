class LoginController < UsersBaseController
  def index
    destroy_session
  end

  def new
    start_session
    if admin?
      redirect_to addresses_url
    else
      redirect_to search_users_path(username: params[:username])
    end
  end
end
