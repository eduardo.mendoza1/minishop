module Admin
  class RegistersController < AdminBaseController
    def new
      @register = Register.new(address: Address.find(params[:address_id]))
    rescue ActiveRecord::RecordNotFound
      redirect_to '/404'
    end

    def create
      @register = Register.new(register_params)

      if @register.save
        redirect_to addresses_url
      else
        render 'new'
      end
    end

    private

    def register_params
      params.require(:register).permit(:meter_number, :register_number, :register_decimals, :address_id)
    end
  end
end
