module Admin
  class UsersController < AdminBaseController
    def update
      @user = User.find(params[:user][:id])
      @user.status = params[:user][:status]

      @user.save
      redirect_to addresses_url
    end
  end
end
