module Admin
  class BillsController < AdminBaseController
    def new
      @bill = Bill.new(user: User.find(params[:user]))
      FixBill.new(@bill).call
    rescue ActiveRecord::RecordNotFound
      redirect_to '/404'
    end

    def create
      @bill = Bill.new(bill_params)
      FixBill.new(@bill).call

      if params[:commit] && @bill.save
        @bill.make_me_the_last
        redirect_to addresses_url
      else
        render 'new'
      end
    end

    private

    def bill_params
      params.require(:bill).permit(:kwh_billed, :billed_at, :user_id)
    end
  end
end
