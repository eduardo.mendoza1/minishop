module Admin
  class PricesController < AdminBaseController
    def new
      @price = Price.new
    end

    def create
      @price = Price.new(price_params)

      if @price.save
        redirect_to addresses_url
      else
        render 'new'
      end
    end

    private

    def price_params
      params.require(:price).permit(:price_value, :valid_since)
    end
  end
end
