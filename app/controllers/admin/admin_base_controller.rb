module Admin
  class AdminBaseController < UsersBaseController
    before_action :check_admin_user

    def check_admin_user
      redirect_to '/404' unless admin?
    end
  end
end
