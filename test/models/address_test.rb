require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  test 'should not save address without ICP' do
    address = Address.new
    assert_not address.save
  end
end
