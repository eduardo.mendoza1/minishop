require 'test_helper'

class AddressesControllerTest < ActionDispatch::IntegrationTest
  test "#index should get 404 redirection without session" do
    get addresses_url
    assert_response :redirect
    assert_redirected_to '/404'
  end
  test "#index should get 404 redirection with no admin user" do
    login_as('user02')

    get addresses_url
    assert_response :redirect
    assert_redirected_to '/404'
  end
  test "#index should get addresses as admin user" do
    login_as('admin')

    get addresses_url
    assert_response :success
    assert_equal '/addresses', path
  end
end
