require 'test_helper'

class BillsControllerTest < ActionDispatch::IntegrationTest
  test '#new is invalid without admin user' do
    user = User.find_by username: 'user02'

    get new_bill_path, params: { user: user.id }
    assert_response :redirect
    assert_redirected_to '/404'
  end

  test '#new is valid with admin user' do
    login_as('admin')
    user = User.find_by username: 'user02'

    get new_bill_path, params: { user: user.id }
    assert_response :success
    assert_equal '/bills/new', path
  end

  test '#create is invalid without admin user' do
    user = User.find_by username: 'user02'

    post bills_url, params: { bill: { kwh_billed: 0, billed_at: 'Tue, 04 Feb 2020 19:21:30 UTC +00:00', user_id: user.id } }
    assert_response :redirect
    assert_redirected_to '/404'
  end

  test '#create is valid with admin user' do
    login_as('admin')
    user = User.find_by username: 'user02'

    post bills_url, params: { bill: { kwh_billed: 0, billed_at: 'Tue, 04 Feb 2020 19:21:30 UTC +00:00', user_id: user.id } }
    assert_response :success
    assert_equal '/bills', path
  end
end
