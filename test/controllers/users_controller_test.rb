require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  test '#new is valid' do
    get new_user_path
    assert_response :success
  end

  test "#create is valid" do
    post users_url, params: { user: { username: 'mock_user' } }
    assert_response :success
    assert_equal '/users', path
  end

  test "#update is invalid without admin user" do
    user = User.find_by username: 'user02'
    patch user_url(id: user.id), params: { user: { id: user.id, status: true } }
    assert_response :redirect
    assert_redirected_to '/404'
  end

  test "#update is valid with admin user" do
    login_as('admin')

    user = User.find_by username: 'user02'
    patch user_url(id: user.id), params: { user: { id: user.id, status: true } }
    assert_response :redirect
    assert_redirected_to '/addresses'
  end

  test "#show is invalid without logged user" do
    user = User.find_by username: 'user02'
    get user_url(id: user.id)
    assert_response :redirect
    assert_redirected_to '/422'
  end
  test "#show is valid with logged user" do
    login_as('user02')

    user = User.find_by username: 'user02'
    get user_url(id: user.id)
    assert_response :success
    assert_equal "/users/#{user.id}", path
  end

  test "#search is invalid without logged user" do
    user = User.find_by username: 'user02'
    get search_users_url(id: user.id)
    assert_response :redirect
    assert_redirected_to '/422'
  end

  test "#search is valid with logged user" do
    login_as('user02')

    user = User.find_by username: 'user02'
    get search_users_url(username: user.username)
    assert_response :redirect
    assert_redirected_to "/users/#{user.id}", path
  end
end
