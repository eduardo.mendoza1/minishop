require 'test_helper'

class RegistersControllerTest < ActionDispatch::IntegrationTest
  test '#new is invalid without admin user' do
    address = Address.find_by icp: '000000001'

    get new_register_path, params: { address_id: address.id }
    assert_response :redirect
    assert_redirected_to '/404'
  end

  test '#new is valid with admin user' do
    login_as('admin')
    address = Address.find_by icp: '000000001'

    get new_register_path, params: { address_id: address.id }
    assert_response :success
    assert_equal '/registers/new', path
  end

  test '#create is invalid without admin user' do
    address = Address.find_by icp: '000000001'

    post registers_url, params: { register: { meter_number: 'ABCDE1234567890', register_number: '2B', address_id: address.id } }
    assert_response :redirect
    assert_redirected_to '/404'
  end

  test '#create is valid with admin user' do
    login_as('admin')
    address = Address.find_by icp: '000000001'

    post registers_url, params: { register: { meter_number: 'ABCDE1234567890', register_number: '2B', address_id: address.id } }
    assert_response :redirect
    assert_redirected_to '/addresses'
  end
end
