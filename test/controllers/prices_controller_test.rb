require 'test_helper'

class PricesControllerTest < ActionDispatch::IntegrationTest
  test '#new is invalid without admin user' do
    get new_price_path
    assert_response :redirect
    assert_redirected_to '/404'
  end

  test '#new is valid with admin user' do
    login_as('admin')

    get new_price_path
    assert_response :success
    assert_equal '/prices/new', path
  end

  test '#create is invalid without admin user' do
    post prices_url, params: { price: { price_value: 0, valid_since: 'Tue, 04 Feb 2020 19:21:30 UTC +00:00' } }
    assert_response :redirect
    assert_redirected_to '/404'
  end

  test '#create is valid with admin user' do
    login_as('admin')

    post prices_url, params: { price: { price_value: 0, valid_since: 'Tue, 04 Feb 2020 19:21:30 UTC +00:00' } }
    assert_response :redirect
    assert_redirected_to '/addresses'
  end
end
