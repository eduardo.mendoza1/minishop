require 'test_helper'

class ReadingsControllerTest < ActionDispatch::IntegrationTest
  test '#new is invalid without admin user' do
    register = Register.find_by address_id: (Address.find_by icp: '000000001').id

    get new_reading_path, params: { register_id: register.id }
    assert_response :redirect
    assert_redirected_to '/422'
  end

  test '#new is valid with admin user' do
    login_as('admin')
    register = Register.find_by address_id: (Address.find_by icp: '000000001').id

    get new_reading_path, params: { register_id: register.id }
    assert_response :success
    assert_equal '/readings/new', path
  end

  test '#create is invalid without admin user' do
    register = Register.find_by address_id: (Address.find_by icp: '000000001').id

    post readings_url, params: { reading: { kwh: 5, read_at: 'Tue, 04 Feb 2020 19:21:31 UTC +00:00', register_id: register.id } }
    assert_response :redirect
    assert_redirected_to '/422'
  end

  test '#create is valid with admin user' do
    login_as('admin')
    register = Register.find_by address_id: (Address.find_by icp: '000000001').id

    post readings_url, params: { reading: { kwh: 5, read_at: 'Tue, 04 Feb 2020 19:21:31 UTC +00:00', register_id: register.id } }
    assert_response :redirect
    assert_redirected_to '/addresses'
  end
end
