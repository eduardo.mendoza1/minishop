# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_08_011337) do

  create_table "addresses", force: :cascade do |t|
    t.string "number", null: false
    t.string "street", null: false
    t.string "town_suburb"
    t.string "region_city"
    t.integer "postcode", null: false
    t.string "icp", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["icp"], name: "index_addresses_on_icp", unique: true
  end

  create_table "bills", force: :cascade do |t|
    t.decimal "kwh_billed", precision: 15, scale: 4, null: false
    t.datetime "billed_at", precision: 6, null: false
    t.boolean "status", default: true, null: false
    t.integer "user_id", null: false
    t.integer "price_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["price_id"], name: "index_bills_on_price_id"
    t.index ["user_id"], name: "index_bills_on_user_id"
  end

  create_table "prices", force: :cascade do |t|
    t.decimal "price_value", precision: 15, scale: 4, null: false
    t.datetime "valid_since", precision: 6, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "readings", force: :cascade do |t|
    t.decimal "kwh", precision: 15, scale: 4, null: false
    t.datetime "read_at", precision: 6, null: false
    t.integer "register_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["register_id"], name: "index_readings_on_register_id"
  end

  create_table "registers", force: :cascade do |t|
    t.string "meter_number", limit: 15, null: false
    t.string "register_number", limit: 15, null: false
    t.integer "register_decimals", limit: 1, default: 0
    t.integer "address_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["address_id", "meter_number", "register_number"], name: "unique_meter_index", unique: true
    t.index ["address_id"], name: "index_registers_on_address_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username", null: false
    t.boolean "status"
    t.integer "address_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["address_id"], name: "index_users_on_address_id"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "bills", "prices", on_delete: :cascade
  add_foreign_key "bills", "users", on_delete: :cascade
  add_foreign_key "readings", "registers", on_delete: :cascade
  add_foreign_key "registers", "addresses", on_delete: :cascade
  add_foreign_key "users", "addresses", on_delete: :cascade
end
