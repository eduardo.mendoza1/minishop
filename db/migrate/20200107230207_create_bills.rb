class CreateBills < ActiveRecord::Migration[6.0]
  def change
    create_table :bills do |t|
      t.decimal :kwh_billed, precision: 15, scale: 4, null: false
      t.datetime :billed_at, precision: 6, null: false
      t.boolean :status, default: true, null: false
      t.references :user, null: false, foreign_key: { on_delete: :cascade }
      t.references :price, null: false, foreign_key: { on_delete: :cascade }

      t.timestamps
    end
  end
end
