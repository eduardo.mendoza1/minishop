class CreateReadings < ActiveRecord::Migration[6.0]
  def change
    create_table :readings do |t|
      t.decimal :kwh, precision: 15, scale: 4, null: false
      t.datetime :read_at, precision: 6, null: false
      t.references :register, null: false, foreign_key: { on_delete: :cascade }

      t.timestamps
    end
  end
end
