class CreatePrices < ActiveRecord::Migration[6.0]
  def change
    create_table :prices do |t|
      t.decimal :price_value, precision: 15, scale: 4, null: false
      t.datetime :valid_since, precision: 6, null: false

      t.timestamps
    end
  end
end
