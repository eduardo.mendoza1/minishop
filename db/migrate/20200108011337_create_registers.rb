class CreateRegisters < ActiveRecord::Migration[6.0]
  def change
    create_table :registers do |t|
      t.string :meter_number,  limit: 15, null: false
      t.string :register_number, limit: 15, null: false
      t.integer :register_decimals, limit: 1, default: 0
      t.references :address, null: false, foreign_key: { on_delete: :cascade }

      t.timestamps
    end
    add_index :registers, [:address_id, :meter_number, :register_number], unique: true, name: 'unique_meter_index'
  end
end
