class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.string :number, null: false
      t.string :street, null: false
      t.string :town_suburb
      t.string :region_city
      t.integer :postcode, null: false
      t.string :icp, null: false, index: { unique: true }

      t.timestamps
    end
  end
end
